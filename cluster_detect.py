#!/usr/bin/env python
# coding: utf-8

from time import time 
import os

import numpy as np

from gensim.models.doc2vec import Doc2Vec, TaggedDocument

from nltk import RegexpTokenizer
tokenizer = RegexpTokenizer(r'\w+')

f = ['the', 'of', 'to','and', 'in', 'is', 'for', 'that', 'with', 'this', 'we','are', 'as', 'on', 'you','it', 'can', 'be', 'by', 'an','from','or','have', 'which', 'will', 'using', 'use', 'if', 'one', 'at','more', 'not', 'each','all', 'our', 'used', 'but', 'your','how', 'has', 'these', 'so', 'also','two', 'there', 'example', 'into', 'when', 'other', 'then', 'first','some', 'such','based', 'they', 'was', 'where','what', 'between', 'new', 'like', 'about','than']
def nlp_clean(data):
    new_str = data.lower()
    dlist = tokenizer.tokenize(new_str)
    #put stopwords removal from here
    dlist = [word for word in dlist if len(word)!=1]
    dlist = [word for word in dlist if word not in f]
    return dlist

path = './scrapes_recent/'
topic_tags = [fil[:fil.index('_')] for fil in os.listdir(path)]

mmd = Doc2Vec.load('para_doc2vec100.model')
tags = list(mmd.docvecs.doctags.keys())
mmd.init_sims(replace= True)


topic_count = {}
for topic in topic_tags:
    topic_count[topic] = topic_count.get(topic, 0) + 1

means = {}
i = 0 
for topic in topic_count:
    ww= [0] * 80
    for j in range(topic_count[topic]):
        ww += mmd.docvecs[j+i]
    ww  = ww/topic_count[topic]
    means[topic]= ww/np.linalg.norm(ww)
    i += topic_count[topic]
    
def cluster(vec):
    cos_dist = -1
    group = 'autoencoders'
    for mean in means:
        vv  = np.dot(vec, means[mean])
        if vv >= cos_dist:
            #print(mean)
            cos_dist = vv
            group = mean
    return group
