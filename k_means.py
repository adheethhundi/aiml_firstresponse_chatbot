
import random

import numpy as np
from numpy import linalg
def similarity(vector1,vector2):
    cosV12 = np.dot(vector1, vector2) / (linalg.norm(vector1) * linalg.norm(vector2))
    return cosV12


class SPkMeans(object):
    """K-Means clustering. Uses cosine similarity as the distance function."""

    def __init__(self, k, vectors):
        assert len(vectors) >= k
        self.centers = random.sample(vectors, k)
        self.clusters = [[] for c in self.centers]
        self.vectors = vectors
        self.report = [0 for c in self.vectors]

    def update_clusters(self):
        """Determine which cluster center each `self.vector` is closest to."""
        def closest_center_index(vector):
            """Get the index of the closest cluster center to `self.vector`."""
            similarity_to_vector = lambda center: similarity(center,vector)
            center = max(self.centers, key=similarity_to_vector)
            return self.centers.index((center))

        self.clusters = [[] for c in self.centers]
        for i, vector in enumerate(self.vectors):
            index = closest_center_index(vector)
            self.clusters[index].append(vector)
            self.report[i] = index

    def update_centers(self):
        """Move `self.centers` to the centers of `self.clusters`.
        Return True if centers moved, else False.
        """
        new_centers = []
        for cluster in self.clusters:
            center = [average(ci) for ci in zip(*cluster)]
            new_centers.append(center)

        if new_centers == self.centers:
            return False

        self.centers = new_centers
        return True

    def main_loop(self):
        """Perform k-means clustering."""
        self.update_clusters()
        i = 0 
        while self.update_centers():
            self.update_clusters()
            print("itr " +str(i))
            i += 1


def average(sequence):
    return sum(sequence) / len(sequence)
