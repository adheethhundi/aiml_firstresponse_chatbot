This is an initial attempt of building a ChatBot for TalentSprint ltd. AIML coursework.
The ChatBot's primary skill is to answer the queries raised by the users in their AIML Coursework Webpage.

Chatbot initially takes an input(question) from the user, classifies the question into one of the intents out of the available 28 intents. 
Then it matches the question with the already present Q&A database and fetches the answer.

The doc2vec Model represenations of all the scraped data is present here:
https://drive.google.com/open?id=1vK7bQIp5n_nBkyEwNd9YvLcr5RXImVlo


