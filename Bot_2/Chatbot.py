from Bot_2.generatengrams import *

def ngrams_matcher(inp, topic):
     my_que_data = database('Bot_2/ques/' + topic)
     my_ans_data = database('Bot_2/ans/' + topic)
     sc_que = my_que_data.score(inp)
     sc_ans = my_ans_data.score(inp)

     tot_score = [(sc_ans[i][0] , sc_que[i][1]+sc_ans[i][1]) for i in range(len(sc_ans))]

     best_q = sorted(tot_score, key=lambda tup: tup[1])
     best_ans = best_q[-1][0]
     return best_q, best_ans

