from cluster_detect import cluster, nlp_clean
from gensim.models.doc2vec import Doc2Vec
from Bot_2.Chatbot import ngrams_matcher
import time

modelg = Doc2Vec.load('para_doc2vec100.model')

def ans_produce(query):
    clean_a = nlp_clean(query)

    ans_vec = modelg.infer_vector(clean_a)
    topic = cluster(ans_vec)
    print("Detected topic: ", topic)
    
    list_best,best_ans = ngrams_matcher(query, topic)
    print('getting the required answer...')
    b = (list_best[-5:])
    print('scores are:', b)
    time.sleep(1)
    print('matched  is: ', best_ans)
    pt = 'Bot_2/ans/'+topic + '/' + best_ans +'.txt'
    rd = open(pt , "r", encoding="utf8").read()
    print("Ans: ", rd)
    aaae = input('Bot: Is this what you wanted to hear?[y/n]')

query1 = input('Bot: Hi! I am Intellibot. Ask me some thing...\nUser:')
ans_produce(query1)

stn = input('Bot: Do you want to ask something else?[y/n]').lower()
while(stn == 'y'):
    query = input('Bot: What is it, please ask!\nUser:')
    ans_produce(query)
    stn = input('Bot: Do you want to ask something else?[y/n]').lower()

print('Bot: Bye, have a nice day...')
